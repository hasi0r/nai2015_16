#include "opencv2/opencv.hpp"
#include <iostream>
#include <sys/stat.h>


using namespace cv;
using namespace std;





void detectObject(Mat frame, CascadeClassifier classifier, String windowName);


String cascade40 = "cascade40.xml";
String cascade800 = "cascade800.xml";

CascadeClassifier skullClassifier40;
CascadeClassifier skullClassifier800;

String windowName40 = "Skull detection 40 samples";
String windowName800 = "Skull detection 800 samples";

int main(void)
{
	
	VideoCapture captureWebcam(0);
	


	Mat frame;

	char escCheck = 0;


	if (!skullClassifier40.load(cascade40)) { printf("Cascade40 did not loaded\n"); return -1; };
	if (!skullClassifier800.load(cascade800)) { printf("Cascade800 did not loaded\n"); return -1; };


	while (escCheck != 27 && captureWebcam.isOpened()) {

		bool frameLoadSucces = captureWebcam.read(frame);		

		if (!frameLoadSucces || frame.empty()) {		
			cout << "webcam error\n";		
			break;													
		}
		flip(frame, frame, 1);
		

		detectObject(frame.clone(),skullClassifier800,windowName800);
		detectObject(frame.clone(), skullClassifier40, windowName40);

		escCheck = waitKey(1);
	}	
	return 0;
}
void detectObject(Mat frame,CascadeClassifier classifier, String windowName)
{
	vector<Rect> skulls;
	Mat frame_gray;
		cvtColor(frame, frame_gray, COLOR_BGR2GRAY);

	equalizeHist(frame_gray, frame_gray);
	
	classifier.detectMultiScale(frame_gray, skulls, 1.2, 3, 0, Size(24, 24));
	
	for (size_t i = 0; i < skulls.size(); i++)
	{
		Point center(skulls[i].x + skulls[i].width/2, skulls[i].y + skulls[i].height/2);

		
		ellipse(frame, center, Size(skulls[i].width / 2, skulls[i].height / 2), 0, 0, 360, Scalar(255, 255, 0), 3, 6, 0);
	//	rectangle(frame, center, Size(skulls[i].width, skulls[i].height ), Scalar(0,255,0), 2, 8, 0);

		
	
	}
	
	imshow(windowName, frame);
}